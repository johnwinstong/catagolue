mapping = {
    "Monarchist" : "Discover a new queen bee shuttle variant",
    "Gemini" : "Discover a new twin bees shuttle variant",
    "Hitchhiker" : "Find a natural occurrence of Kok's galaxy",
    "Conchita" : "Find a soup containing a phoenix",
    "Trillionaire" : "Contribute one trillion objects",
    "Gigamyriad" : "Contribute 10^13 objects",
    "Sprotsmanship" : "Contribute one third of a trillion objects to a different rule",
    "Limitless" : "Observe a new natural infinite-growth pattern",
    "Voyager" : "Find one of the first twenty occurrences of a spaceship",
    "Loafer" : "Discover a natural loafer",
}
